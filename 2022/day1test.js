const sh = require('superheroes');
const fs = require('fs');

let input = fs.readFileSync('input.txt').toString();
const lines = input.split('\n');

let arr = [];

var total = 0;

for (var line of lines) {

	line = parseInt(line);

	if(!line) {
		line = 0;
	}

	total += line;

	if(line <= 1) {
		arr.push(total);
		total = 0;
	} 
    
}

arr.sort((a, b) => b - a);

let one = arr[0];
let two = arr[1];
let three = arr[2];

console.log(one + two + three);