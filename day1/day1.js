/** HEADERS **/
const fs = require('fs');

/* PART ONE START 

let input = fs.readFileSync('input.txt').toString();
const lines = input.split('\n');

let arr = [];

var total = 0;

for (var line of lines) {

	var first = null;
	var last;

	for(var x = 0; x < line.length; x++) {

		var char = parseInt(line.charAt(x));

		//console.log(line.charAt(x));

		if(char >= 0) {
			if(!first) {
				first = char.toString();
			}
			last = char.toString();
		}

	}

	//console.log('first: ' + first + '. last: ' + last);

	var tempTotal = first + last;

	total = total + parseInt(tempTotal);
    
}

console.log(total);

/* END PART ONE */

/* PART TWO START */
let input = fs.readFileSync('input.txt').toString();
const lines = input.split('\n');

let arr = [];

var total = 0;

for (var line of lines) {

	var first = null;
	var last;

	for(var x = 0; x < line.length; x++) {

		strNumber = checkNumber(line, line.charAt(x), x);

		var number = parseInt(line.charAt(x));

		if(strNumber !== undefined) {
			if(!first) {
				first = strNumber.toString();
			}
			last = strNumber.toString();
		}

		if(number >= 0) {
			if(!first) {
				first = number.toString();
			}
			last = number.toString();
		}

	}

	//console.log('first: ' + first + '. last: ' + last);

	var tempTotal = first + last;

	total = total + parseInt(tempTotal);
    
}

/* Yes, not the prettiest of functions... */
function checkNumber(line, startChar, pos) {

	if(line.substring(pos, pos+3) == 'one') {
		return 1;
	}
	if(line.substring(pos, pos+3) == 'two') {
		return 2;
	}
	if(line.substring(pos, pos+5) == 'three') {
		return 3;
	}
	if(line.substring(pos, pos+4) == 'four') {
		return 4;
	}
	if(line.substring(pos, pos+4) == 'five') {
		return 5;
	}
	if(line.substring(pos, pos+3) == 'six') {
		return 6;
	}
	if(line.substring(pos, pos+5) == 'seven') {
		return 7;
	}
	if(line.substring(pos, pos+5) == 'eight') {
		return 8;
	}
	if(line.substring(pos, pos+4) == 'nine') {
		return 9;
	}

	return;

}

console.log(total);

/* END PART TWO */